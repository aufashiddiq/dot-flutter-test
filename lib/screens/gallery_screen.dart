
import '../blocs/gallery_bloc.dart';
import '../models/gallery_model.dart';
import '../networking/response.dart';
import '../widgets/gallery_item.dart';
import '../widgets/loading.dart';
import 'package:flutter/material.dart';

class GalleryScreen extends StatefulWidget {
  @override
  _GalleryScreenState createState() => _GalleryScreenState();
}

class _GalleryScreenState extends State<GalleryScreen> {

  GalleryBloc _galleryBloc;
  ScrollController _scrollController;

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController()..addListener(_scrollListener);
    _galleryBloc = GalleryBloc();
  }

  @override
  void dispose() {
    _galleryBloc.dispose();
    super.dispose();
  }

  void _scrollListener() {
    if (_scrollController.position.pixels == _scrollController.position.maxScrollExtent) {
      _galleryBloc.loadMoreGallery();
    }
  }

  @override
  Widget build(BuildContext context) {

    _galleryBloc.context = context;
    return RefreshIndicator(
      onRefresh: () => _galleryBloc.fetchGallery(),
      child: StreamBuilder<Response<List<Gallery>>>(
        stream: _galleryBloc.galleryListStream,
        builder: (context, snapshot) {

          if (snapshot.hasData) {            
            
            return Container(
              child: GridView.builder(
                itemCount: snapshot.data.data.length + 2,
                controller: _scrollController,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  childAspectRatio: 1,
                ),
                itemBuilder: (ctx, i) {
                  return i == snapshot.data.data.length + 1 ?
                    snapshot.data.status != Status.ERROR ? 
                      LoadingWidget() 
                      : Container()
                    : i == snapshot.data.data.length ? 
                      Container() 
                      : GalleryItem(snapshot.data.data[i]);
                },
              ),
            );
          }
          return Container();
        },
      ),
    );
  }
}