import '../screens/information_screen.dart';
import '../screens/version_screen.dart';
import 'package:flutter/material.dart';

class AboutScreen extends StatefulWidget {
  @override
  _AboutScreenState createState() => _AboutScreenState();
}

class _AboutScreenState extends State<AboutScreen>  with SingleTickerProviderStateMixin {

  TabController controller;

  @override
  void initState() {
    super.initState();

    controller = TabController(length: 2, vsync: this);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  TabBar getTabBar() {
    return TabBar(
      tabs: <Tab>[
        Tab(text: "Informasi"),
        Tab(text: "Versi Rilis")
      ],
      isScrollable: true,
      controller: controller,
    );
  }

  TabBarView getTabBarView(var tabs) {
    return TabBarView(
      children: tabs,
      controller: controller,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('About', style: TextStyle(fontFamily: 'PacificoRegular')),
        bottom: getTabBar()
      ),
      body: getTabBarView(<Widget>[
        InformationScreen(), VersionScreen()
      ]),
      
    );
  }
}