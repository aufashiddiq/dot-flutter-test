import 'package:cached_network_image/cached_network_image.dart';

import '../helpers/constants.dart';
import '../models/gallery_model.dart';
import 'package:flutter/material.dart';

class GalleryDetailScreen extends StatelessWidget {

  final Gallery item;

  GalleryDetailScreen(this.item);

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text('Gallery Detail', style: TextStyle(fontFamily: 'PacificoRegular'))
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[

          AspectRatio(
            aspectRatio: 3 / 2,
            child: CachedNetworkImage(
              imageUrl: item.image,
              placeholder: (context, url) => Center(child: CircularProgressIndicator()),
              errorWidget: (context, url, error) => Center(child: Icon(Icons.error)),
              fit: BoxFit.cover,
            )
          ),

          Padding(
            padding: EdgeInsets.all(SpacingConst.smallSpacing),
            child: Text(item.caption, 
              style: TextStyle(
                fontSize: TextSizeConst.regular,
              ),
            ),
          ),

        ],
      ),
    );

  }

}