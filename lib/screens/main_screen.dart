import '../models/menu_model.dart';
import '../screens/about_screen.dart';
import '../screens/gallery_screen.dart';
import '../screens/place_screen.dart';
import '../widgets/bottom_navigation.dart';
import 'package:flutter/material.dart';

class MainScreen extends StatefulWidget {
  static const routeName = '/main_screen';

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {

  final List<MenuModel> menuItems = [
    MenuModel('List Place', Icon(Icons.home), PlaceScreen()),
    MenuModel('List Gallery', Icon(Icons.image), GalleryScreen()),
    MenuModel('About', Icon(Icons.info), AboutScreen()),
  ];

  int selectedIndex = 0;
  
  void onChangeMenu(index) {
    setState(() {
      selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(

      appBar: selectedIndex != 2 ? AppBar(
        title: Text(menuItems[selectedIndex].title, style: TextStyle(fontFamily: 'PacificoRegular'))
      ) : null,

      body: menuItems[selectedIndex].screen,

      bottomNavigationBar: BottomNavigation(
        menuItems: menuItems, 
        selectedIndex: selectedIndex, 
        onChangeMenu: onChangeMenu,  
      )

    );
  }

}