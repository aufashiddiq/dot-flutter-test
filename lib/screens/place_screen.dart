import '../blocs/place_bloc.dart';
import '../models/place_model.dart';
import '../networking/response.dart';
import '../widgets/loading.dart';
import '../widgets/place_item.dart';
import 'package:flutter/material.dart';

class PlaceScreen extends StatefulWidget {
  @override
  _PlaceScreenState createState() => _PlaceScreenState();
}

class _PlaceScreenState extends State<PlaceScreen> {

  PlaceBloc _placeBloc;
  ScrollController _scrollController;
  

  @override
  void initState() {
    super.initState();
    _placeBloc = PlaceBloc();
    _scrollController = ScrollController()..addListener(_scrollListener);
    
  }

  @override
  void dispose() {
    _placeBloc.dispose();
    super.dispose();
  }

  void _scrollListener() {
    if (_scrollController.position.pixels == _scrollController.position.maxScrollExtent) {
      _placeBloc.loadMorePlace();
    }
  }

  @override
  Widget build(BuildContext context) {
    
    _placeBloc.context = context;
    return RefreshIndicator(
      onRefresh: () => _placeBloc.fetchPlace(),
      child: StreamBuilder<Response<List<Place>>>(
        stream: _placeBloc.placeListStream,
        builder: (context, snapshot) {

          if (snapshot.hasData) {

            return Container(
              child: ListView.builder(
                itemCount: snapshot.data.data.length + 1,
                shrinkWrap: true,
                controller: _scrollController,
                itemBuilder: (context, index) { 
                  return index == snapshot.data.data.length ?
                    snapshot.data.status != Status.ERROR ? 
                      LoadingWidget() 
                      : Container()
                    : PlaceItem(snapshot.data.data[index]);
                },
              ),
            );

          }
          
          return Container();
        },
      ),
    );
  }
}