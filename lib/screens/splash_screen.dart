import 'dart:async';
import '../helpers/constants.dart';
import '../screens/main_screen.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  
  startTime() async {
    var _duration = new Duration(seconds: 2);
    return new Timer(_duration, navigateToMain);
  }

  void navigateToMain() {
    Navigator.of(context).pushReplacementNamed(MainScreen.routeName);
  }

  @override
  void initState() {
    super.initState();
    startTime();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(SpacingConst.regularSpacing),
        color: Colors.teal,
        child: Stack(
          children: <Widget>[

            Align(
              alignment: Alignment.center,
              child: Center(
                child: Text(
                  'DOT Flutter Test', 
                  style: TextStyle(
                    color: Colors.white,
                    fontFamily: 'PacificoRegular',
                    fontSize: TextSizeConst.large
                  )
                )
              )
            ),

            Align(
              alignment: Alignment.bottomCenter, 
              child: Text('Versi 1.0.0', 
                style: TextStyle(
                  fontSize: TextSizeConst.regular, 
                  color: Colors.white
                )
              ),
            )

          ],
        )
      )
    );
    
  }
}