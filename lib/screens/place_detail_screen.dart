import 'package:cached_network_image/cached_network_image.dart';

import '../helpers/constants.dart';
import '../models/place_model.dart';
import 'package:flutter/material.dart';

class PlaceDetailScreen extends StatelessWidget {

  final Place item;

  PlaceDetailScreen(this.item);

  @override
  Widget build(BuildContext context) {

    return Scaffold(

      appBar: AppBar(
        title: Text('Place Detail', style: TextStyle(fontFamily: 'PacificoRegular'))
      ),

      body: Column(
        children: <Widget>[

          AspectRatio(
            aspectRatio: 3 / 2,
            child: CachedNetworkImage(
              imageUrl: item.image,
              placeholder: (context, url) => Center(child: CircularProgressIndicator()),
              errorWidget: (context, url, error) => Icon(Icons.error),
              fit: BoxFit.cover,
            ),
          ),

          Container(
            padding: EdgeInsets.all(SpacingConst.regularSpacing),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[

                Padding(
                  padding: EdgeInsets.only(bottom: SpacingConst.smallSpacing),
                  child: Text(item.name, 
                    style: TextStyle(
                      fontSize: TextSizeConst.large,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),

                Text(item.description, 
                  style: TextStyle(
                    fontSize: TextSizeConst.regular,
                  )
                )

              ],
            ),
          )
        ],
      ),
    );

  }
}