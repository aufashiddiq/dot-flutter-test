// URL Constants
mixin URLs {
  static const baseUrl = "https://dot-test-70d73.firebaseapp.com/";
}


// Text Size
mixin TextSizeConst {

  static const verySmall = 8.0;
  static const small = 12.0;
  static const regular = 16.0;
  static const medium = 20.0;
  static const large = 24.0;
  static const veryLarge = 30.0;

}

// Constant for App Spacing
mixin SpacingConst {
  
  static const verySmallSpacing = 6.0;
  static const smallSpacing = 10.0;
  static const regularSpacing = 14.0;
  static const mediumSpacing = 18.0;
  static const largeSpacing = 22.0;
  static const veryLargeSpacing = 26.0;

}