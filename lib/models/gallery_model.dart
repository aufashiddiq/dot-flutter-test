
class GalleryResponse {

  final int status;
  final List<Gallery> data;

  GalleryResponse({this.status, this.data});

  factory GalleryResponse.fromJson(Map<String, dynamic> json) {

    List dataList = json['data'];
    return GalleryResponse(
      data: dataList.map((model) => Gallery.fromJson(model)).toList(),
      status: json['created_at'],
    );
  }

}

class Gallery {

  final String caption;
  final String thumbnail;
  final String image;

  Gallery({this.caption, this.thumbnail, this.image});

  factory Gallery.fromJson(Map<String, dynamic> json) {
    return Gallery(
      caption: json['caption'],
      thumbnail: json['thumbnail'],
      image: json['image']
    );
  }

  Map<String, dynamic> toJson() => {
    "caption": this.caption,
    "thumbnail": this.thumbnail,
    "image": this.image,
  };

}