import 'package:flutter/widgets.dart';

class MenuModel {
  
  final String title;
  final Icon icon;
  final Widget screen;

  MenuModel(
    this.title,
    this.icon,
    this.screen
  );

}