
class PlaceResponse {

  final int status;
  final List<Place> data;

  PlaceResponse({this.status, this.data});

  factory PlaceResponse.fromJson(Map<String, dynamic> json) {

    List dataList = json['data'];
    return PlaceResponse(
      data: dataList.map((model) => Place.fromJson(model)).toList(),
      status: json['created_at'],
    );
  }

}

class Place {

  final String name;
  final String location;
  final String description;
  final String image;

  Place({this.name, this.location, this.description, this.image});

  factory Place.fromJson(Map<String, dynamic> json) {
    return Place(
      name: json['name'],
      location: json['location'],
      description: json['description'],
      image: json['image']
    );
  }

  Map<String, dynamic> toJson() => {
    "name": this.name,
    "location": this.location,
    "description": this.description,
    "image": this.image,
  };

}