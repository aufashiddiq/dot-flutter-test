import 'package:dot_flutter_test/dao/gallery_dao.dart';

import '../models/gallery_model.dart';
import '../networking/api_provider.dart';

class GalleryRepository {
  ApiProvider _apiProvider = ApiProvider();
  final galleryDao = GalleryDao();
  var galleryList;
  int _page = 0;

  Future<List<Gallery>> fetchGalleryData(int page) async {

    print(page%3);
    if (page % 3 == 0) {
      
      _page = 0;
      final response = await _apiProvider.get('list_gallery.json');
      GalleryResponse objResponse = GalleryResponse.fromJson(response);
      galleryList = objResponse.data;

    }
    
    _page++;
    switch (_page) {

      case 1:
        return galleryList.sublist(0, 15);
        break;

      case 2:
        await Future.delayed(Duration(seconds: 1));
        return galleryList.sublist(15, 30);
        break;

      default:
        await Future.delayed(Duration(seconds: 1));
        return galleryList.sublist(30, 45);
        break;

    }
    
  }

  Future getGalleries() => galleryDao.getGalleries();

  Future insertGalleries(List<Gallery> galleryList) => galleryDao.insertGalleries(galleryList);

  Future deleteGalleries() => galleryDao.deleteGalleries();

}