import 'package:dot_flutter_test/dao/place_dao.dart';

import '../models/place_model.dart';
import '../networking/api_provider.dart';

class PlaceRepository {
  ApiProvider _apiProvider = ApiProvider();
  final placeDao = PlaceDao();

  Future<List<Place>> fetchPlaceData(int page) async {
    final response = await _apiProvider.get('list_place.json');
    PlaceResponse objResponse = PlaceResponse.fromJson(response);
    return objResponse.data;
  }

  Future getPlaces() => placeDao.getPlaces();

  Future insertPlaces(List<Place> place) => placeDao.insertPlaces(place);

  Future deletePlaces() => placeDao.deletePlaces();

}