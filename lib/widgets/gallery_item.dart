import 'package:cached_network_image/cached_network_image.dart';

import '../models/gallery_model.dart';
import '../screens/gallery_detail_screen.dart';
import 'package:flutter/material.dart';

class GalleryItem extends StatelessWidget {

  final Gallery item;

  GalleryItem(this.item);

  @override
  Widget build(BuildContext context) {

    return GridTile(
      child: GestureDetector(

        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => GalleryDetailScreen(item),
            ),
          );
        },

        child: CachedNetworkImage(
          imageUrl: item.image,
          placeholder: (context, url) => Center(child: CircularProgressIndicator()),
          errorWidget: (context, url, error) => Center(child: Icon(Icons.error)),
          fit: BoxFit.cover,
        ),

      )
    );

  }
}