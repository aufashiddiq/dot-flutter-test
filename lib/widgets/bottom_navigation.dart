import '../models/menu_model.dart';
import 'package:flutter/material.dart';

class BottomNavigation extends StatelessWidget {

  final List<MenuModel> menuItems;
  final int selectedIndex;
  final Function onChangeMenu;

  BottomNavigation({this.menuItems, this.selectedIndex, this.onChangeMenu});

  @override
  Widget build(BuildContext context) {

    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      items: menuItems.map((item) {

        return BottomNavigationBarItem(
          icon: item.icon,
          title: Text(item.title),
        );

      }).toList(),
      currentIndex: selectedIndex,
      selectedItemColor: Colors.teal,
      onTap: onChangeMenu,
    );

  }

}