import 'package:cached_network_image/cached_network_image.dart';

import '../helpers/constants.dart';
import '../models/place_model.dart';
import '../screens/place_detail_screen.dart';
import 'package:flutter/material.dart';

class PlaceItem extends StatelessWidget {

  final Place item;

  PlaceItem(this.item);

  @override
  Widget build(BuildContext context) {

    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => PlaceDetailScreen(item),
          ),
        );
      },
      
      child: Container(
        child: Stack(
          children: <Widget>[

            AspectRatio(
              aspectRatio: 2 / 1,
              child: CachedNetworkImage(
                imageUrl: item.image,
                placeholder: (context, url) => Center(child: CircularProgressIndicator()),
                errorWidget: (context, url, error) => Center(child: Icon(Icons.error)),
                fit: BoxFit.cover,
              ),
            ),

            AspectRatio(
              aspectRatio: 2 / 1,
              child: Container(

                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter, 
                    colors: [const Color(0x00000000), const Color(0x88000000)],
                    tileMode: TileMode.repeated,
                  ),
                ),

                padding: EdgeInsets.all(SpacingConst.regularSpacing),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[

                    Padding(
                      padding: EdgeInsets.only(bottom: SpacingConst.smallSpacing),
                      child: Text(item.name, 
                        style: TextStyle(
                          fontSize: TextSizeConst.medium,
                          fontWeight: FontWeight.bold,
                          color: Colors.white
                        )
                      ),
                    ),

                    Text(item.location, 
                      style: TextStyle(
                        fontSize: TextSizeConst.regular,
                        color: Colors.white
                      )
                    )

                  ],
                )
              )
            )
          ],
        )
      )
    );

  }
  
}