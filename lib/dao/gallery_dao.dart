import 'package:dot_flutter_test/database/database.dart';
import 'package:sqflite/sqflite.dart';
import '../models/gallery_model.dart';

class GalleryDao {

  final dbProvider = DatabaseProvider.dbProvider;

  insertGalleries (List<Gallery> galleryList) async {
    final db = await dbProvider.database;
    Batch batch = db.batch();

    galleryList.forEach((gallery) async {
      batch.insert(galleryTable, gallery.toJson());
    });
    await batch.commit(noResult: true);
    
  }

  Future<List<Gallery>> getGalleries() async {
    final db = await dbProvider.database;

    List<Map<String, dynamic>> result;
    
      result = await db.query(galleryTable);

    List<Gallery> galleries = result.isNotEmpty
        ? result.map((item) => Gallery.fromJson(item)).toList()
        : [];
    return galleries;
  }

  Future deleteGalleries() async {
    final db = await dbProvider.database;
    var result = await db.delete(
      galleryTable,
    );

    return result;
  }

}