import 'package:dot_flutter_test/database/database.dart';
import 'package:sqflite/sqflite.dart';
import '../models/place_model.dart';

class PlaceDao {

  final dbProvider = DatabaseProvider.dbProvider;

  insertPlaces(List<Place> places) async {

    final db = await dbProvider.database;
    Batch batch = db.batch();

    places.forEach((place) async {
      batch.insert(placeTable, place.toJson());
    });
    await batch.commit(noResult: true);
    
  }

  Future<List<Place>> getPlaces() async {
    final db = await dbProvider.database;

    List<Map<String, dynamic>> result;
    
    result = await db.query(placeTable);

    List<Place> places = result.isNotEmpty
        ? result.map((item) => Place.fromJson(item)).toList()
        : [];
    return places;
  }

  Future deletePlaces() async {
    final db = await dbProvider.database;
    var result = await db.delete(
      placeTable,
    );

    return result;
  }

}