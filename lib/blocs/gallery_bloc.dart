import 'dart:async';

import 'package:flutter/material.dart';

import '../models/gallery_model.dart';
import '../networking/response.dart';
import '../repository/gallery_repository.dart';

class GalleryBloc {

  List<Gallery> _galleryList;
  var context;
  int _page = 0;  

  GalleryRepository _galleryRepository;
  StreamController _galleryController;

  StreamSink<Response<List<Gallery>>> get galleryListSink => _galleryController.sink;
  Stream<Response<List<Gallery>>> get galleryListStream => _galleryController.stream;

  GalleryBloc() {
    _galleryController = StreamController<Response<List<Gallery>>>();
    _galleryRepository = GalleryRepository();
    fetchGallery();
  }

  fetchGallery() async {

    _page = 0;
    _galleryList = await _galleryRepository.getGalleries();
    galleryListSink.add(Response.loading('Getting Gallery...', _galleryList));
    var localGalleryLength = _galleryList.length;
    print('Local Gallery Data Length => $localGalleryLength');
    
    try {

      _galleryList = await _galleryRepository.fetchGalleryData(_page);
      galleryListSink.add(Response.completed(_galleryList));

    } catch (e) {

      galleryListSink.add(Response.error(e.toString(), _galleryList));
      _errorSnackBar(e.toString());
      print(e);

    } finally {

      await _galleryRepository.deleteGalleries();
      await _galleryRepository.insertGalleries(_galleryList);

    }

  }

  loadMoreGallery() async {

    try {

      _page++;
      _galleryList.addAll(await _galleryRepository.fetchGalleryData(_page));
      galleryListSink.add(Response.completed(_galleryList));
      print('Gallery Pagination => $_page');
      
    } catch (e) {

      galleryListSink.add(Response.error(e.toString(), _galleryList));
      _errorSnackBar(e.toString());
      print(e);

    }

  }

  _errorSnackBar(message) {

    final snackBar = SnackBar(
      content: Text(message),
      action: SnackBarAction(
        label: 'Retry',
        onPressed:() => fetchGallery(),
      ),
    );
    Scaffold.of(context).showSnackBar(snackBar);

  }

  dispose() {
    _galleryController?.close();
  }

}