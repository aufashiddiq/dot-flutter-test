import 'dart:async';

import 'package:flutter/material.dart';

import '../models/place_model.dart';
import '../networking/response.dart';
import '../repository/place_repository.dart';

class PlaceBloc {

  var context;
  List<Place> _placeList;
  int _page = 0;  

  PlaceRepository _placeRepository;
  StreamController _placeController;

  StreamSink<Response<List<Place>>> get placeListSink => _placeController.sink;
  Stream<Response<List<Place>>> get placeListStream => _placeController.stream;

  PlaceBloc() {
    _placeController = StreamController<Response<List<Place>>>();
    _placeRepository = PlaceRepository();
    fetchPlace();
  }

  fetchPlace() async {

    _page = 0;
    _placeList = await _placeRepository.getPlaces();
    placeListSink.add(Response.loading('Getting Place...', _placeList));
    var localPlaceLength = _placeList.length;
    print('Local Place Data Length => $localPlaceLength');

    try {

      _placeList = await _placeRepository.fetchPlaceData(_page);
      placeListSink.add(Response.completed(_placeList));

    } catch (e) {

      placeListSink.add(Response.error(e.toString(), _placeList));
      _errorSnackBar(e.toString());
      print(e);

    } finally {

      await _placeRepository.deletePlaces();
      await _placeRepository.insertPlaces(_placeList);

    }

  }

  loadMorePlace() async {

    try {

      _page++;
      _placeList.addAll(await _placeRepository.fetchPlaceData(_page));
      placeListSink.add(Response.completed(_placeList));
      print('Place Pagination => $_page');
      
    } catch (e) {

      placeListSink.add(Response.error(e.toString(), _placeList));
      _errorSnackBar(e.toString());
      print(e);

    }

  }

  _errorSnackBar(message) {

    final snackBar = SnackBar(
      content: Text(message),
      action: SnackBarAction(
        label: 'Retry',
        onPressed:() => fetchPlace(),
      ),
    );
    Scaffold.of(context).showSnackBar(snackBar);

  }

  dispose() {
    _placeController?.close();
  }

}