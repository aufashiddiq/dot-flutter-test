# DOT Flutter Test

### **Knowledge & Experience Question & Answers**

**1. Sudah berapa lama anda mulai menggunakan Flutter?**

Sejak bulan januari 2020

**2. Apa saja library Flutter yang sering atau biasa anda pakai? Jelaskan kegunaannya bagi anda.**

* Flutter Http: digunakan untuk akses Internet/API dalam aplikasi.

**3. Jelaskan penerapan clean code atau design pattern yang biasa anda gunakan dan manfaatnya untuk anda.**

* Melakukan pemisahan widget-widget yang digunakan berulang ke dalam satu file widget agar tidak redundan. Serta membuat file constant yang berisi konstanta seperti url, appspacing, textsize dll. Tujuannya agar styling lebih rapi. dan dapat digunakan berulang-ulang. dan apabila terdapat perubahan style hanya mengubah nilai di file constant saja.

* Diusahakan dalam satu file tidak lebih dari 200 baris kode program.

* Untuk design pattern, saya menggunakan Provider Design Patterns, karena manajemen statenya terpusat, dan apabila terjadi perubahan state, maka view otomatis menyesuaikan.

**4. Apakah tantangan terbesar yang pernah anda temui dalam mengerjakan aplikasi Flutter dan bagaimana anda menyelesaikannya dari sisi engineering? Misal dari sisi Android dan iOS.**

Styling, karena styling dalam flutter lebih kompleks dari platform yang pernah saya pakai seperti ReactNative & Kotlin. Untuk menyelesaikannya saya mencari contoh implementasinya di internet, kemudian menerapkannya di aplikasi sendiri kemudian dimodifikasi.

**5. Untuk efisiensi pengerjaan project dalam tim, bagaimana workflow anda dari proses development hingga merilis aplikasi hingga bisa digunakan oleh tester / client?**

* Breakdown fitur2 aplikasi yg akan dikerjakan.
* Merancang base & rule aplikasi (architecture, models, constant, general style, components dll) oleh project manager.
* Menjelaskan base aplikasi kepada tim dan bagi tugas.
* Tim harus menggunakan base & rule yang telah dibuat oleh project manager, agar efisien.
* Setelah aplikasi selesai, kemudian dilakukan automation testing.
* Apabila masih ditemukan bug/kesalahan, maka selanjutnya dilakukan perbaikan.
* Apabila aplikasi sudah tidak ada bug/kesalahan, maka aplikasi dapat digunakan oleh tester/client.

**6. Jelaskan teknik-teknik apa saja yang dapat meningkatkan performance dan keamanan sebuah aplikasi Flutter.**

* Usahakan widget yang digunakan berulang-ulang dipisah per component/item. agar kodingan tidak repetitif & tidak memakan size yg besar.
* Method setState() usahakan dipanggil di widget yang datanya dapat berubah-ubah, tidak dipanggil di parent widget.
* Untuk keamanan, usahakan sdk dan dependency yang digunakan adalah versi yang terbaru.

**7. Apakah anda bersedia onsite di Malang?**

Bersedia.